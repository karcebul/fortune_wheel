$(document).ready(function () {
  var solutions = ["Raz kozie śmierć", "Dla chcącego nic trudnego", "Do trzech razy sztuka"];
  
  var badGuessCount = 0;
  var maxWrongGuess = 5;
  
  function pickSolution(tab) {
    return tab[Math.floor(Math.random() * tab.length)].toLowerCase();
  }
  
  var board = $('.js-board');
  var letterExample = $('.js-letter');
  var wordsContainer = $('.js-word');
  
  function printSolution(sol){
    board.empty();
    let words = sol.split(' ');
    let newLetter = letterExample.clone();
    for (let j = 0; j < words.length; j++){
      let newWord = wordsContainer.clone().empty();
      for(let i = 0; i < words[j].length; i++){
        newLetter = letterExample.clone();
        console.log(words[j][i])
        newWord.append(newLetter.text(words[j][i]));  
      }
      newLetter = letterExample.clone();
      if(j !== words.length - 1){
        newWord.append(newLetter.text(' ').addClass('space'));
      }
      board.append(newWord);
    }
  }
  let currentSolution = pickSolution(solutions)
  printSolution(currentSolution);
  let numberOfSpaces = currentSolution.split(' ').length - 1;
  
  let input = $('.js-input');
  let usedLetters = [];
  let usedUp = $('.usedUp')
  input.on('input', function(){
    let actualValue = $(this).val().toLowerCase();
    //let actualValue = e.key.toLowerCase();
    
    //console.log(actualValue, e);
    //console.log(e.key);
    usedLetters.push(actualValue + " ");
    $(".usedUp").empty();
    $(".usedUp").append(usedLetters);
    
    checkLetter(actualValue);
    checkSucces();
    input.val('');
  })
  
  function checkLetter(guess){
    let letters = $('.js-word').children('.js-letter');
    let letterExists = false;
    let guessed = 0;
    let currentLetter = '';
    
    letters.each(function(){
      if($(this).text().toLowerCase() === guess){
        if($(this).hasClass('folded')){
          $(this).removeClass('folded');
          guessed++;
        }
        letterExists = true;
        currentLetter = $(this).text().toLowerCase();
      }
    });
    
    if(letterExists === false){
      addMessage("Bledna litera!", ".js-log");
      badGuessCount++;
      if(badGuessCount >= maxWrongGuess){
        changeMessage("You have lost :(")
        disableInput();
      }
    }
    else if (letterExists === true && guessed === 0){
      addMessage("Litera byla juz zgadnieta!", ".js-log")
    }
    else {
      addMessage("Zgadles litere " + currentLetter + " Odslonieto " + guessed + " liter.", ".js-log");
    }
    
  }
  
  let guessPass = $(".js-pass");
  
  guessPass.on('change', function(){
    let guessedPass = $(this).val().toLowerCase();
    console.log(guessedPass, currentSolution.toLowerCase())
    if(guessedPass === currentSolution.toLowerCase()){
      console.log($('.js-word').children('.js-letter'))
      $('.js-word').children('.js-letter:not(.space)').removeClass('folded');
      checkSucces();
    }
  });
  
  
  function checkSucces(){
    if($(".folded").length === numberOfSpaces){
      changeMessage("Brawo Ty!");
      disableInput();
    }
  }
  
  
  function disableInput(){
    $("input").each(function(){
      $(this).attr("disabled", true)
    })
  }
  
  function changeMessage(msg){
    $('.js-status').empty();
    $('.js-status').append(msg);
    $('.js-status').append($('</br>'));
  }
  
  function addMessage(msg, where){
    $(where).append(msg);
    $(where).append($('</br>'));
  }
  
})
